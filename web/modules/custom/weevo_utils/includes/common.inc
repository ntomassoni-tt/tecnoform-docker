<?php

use \Drupal\Core\Security\TrustedCallbackInterface;

/**
 * Implements hook_toolbar().
 * Create local tasks toolbar items
 */
function weevo_utils_toolbar() {

  $callback = function() {
    // Get primary local task links and inject them into new
    // admin_toolbar_local_tasks toolbar tray.
    $manager = \Drupal::service('plugin.manager.menu.local_task');
    $links = $manager->getLocalTasks(\Drupal::routeMatch()->getRouteName(), 0);
    if (!empty($links)) {
      $build['#theme'] = 'links';
      $build['#attributes'] = ['class' => ['toolbar-menu']];
      foreach ($links['tabs'] as $route => $link) {
        $build['#links'][$route] = $link['#link'];
      }
      return $build;
    }
    return [];    
  };
  
  $items = [];

  // Toolbar item for primary local tasks.
  // @todo - do we need some configuration to turn this on/off?
  $items['admin_toolbar_local_tasks'] = [
    '#type' => 'toolbar_item',
    '#wrapper_attributes' => [
      'class' => ['local-tasks-toolbar-tab'],
    ],
    // Put it after contextual toolbar item so when float right is applied
    // local tasks item will be first.
    '#weight' => 10,
    'tab' => [
      // We can't use #lazy_builder here because
      // ToolbarItem::preRenderToolbarItem will insert #attributes before
      // lazy_builder callback and this will produce Exception.
      // This means that for now we always render Local Tasks item even when
      // the tray is empty.
      '#type' => 'link',
      '#title' => t('Local Tasks'),
      '#cache' => [
        'max-age' => 0,
      ],
      '#url' => \Drupal\Core\Url::fromRoute('<none>'),
      '#attributes' => [
        'class' => [
          'toolbar-icon',
          'toolbar-icon-shortcut',
        ],
      ],
    ],
    'tray' => [
      'local_links' => [
        '#lazy_builder' => [$callback, []],
      ],
    ],
    '#attached' => ['library' => ['admin_toolbar_tools/toolbar.icon']],
  ];

  return $items;
}

