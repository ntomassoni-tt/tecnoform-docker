<?php

use \Drupal\views\ViewExecutable;
use \Drupal\views\Plugin\views\query\QueryPluginBase;
use \Drupal\node\Entity\Node;

/**
 * hook_views_views_query_alter()
 *
 * @param ViewExecutable $view
 * @param QueryPluginBase $query
 */
function weevo_utils_views_query_alter(ViewExecutable $view, QueryPluginBase $query) {
  $viewId       = (string)$view->id();
  $viewDisplay  = (string)$view->current_display;

  // Vista Prodotti, display Ricerca prodotti
  /*
  if($viewId === 'prodotti' && $viewDisplay === 'product_search') {
    foreach($query->where as $cgIndex=>&$condition_group) {
      _productSearch_recursion_conditions($query, $condition_group['conditions'], $cgIndex);
    }
  }
   */
}

/**
 * Vista Prodotti, display Ricerca prodotti (/it/admin/structure/views/view/prodotti)
 * Itera e trasforma la sola condizione sulla tassonomia "categorie_cuscinetti"
 *
 * Se trova il filtro impostato
 * - rimuove la condizione standard
 * - ricerca tutti i figli del termine attuale e genera l'array complessivo di tid
 * - imposta un nuovo gruppo di ricerca con una ricerca su tutti i tid trovati
 *
 * E' necessario rimuovere il vecchio filtro per evitare condizione di OR su valori non coerenti.
 *
 * @param $query
 * @param $conditions
 * @param $cgIndex
 */
function _productSearch_recursion_conditions(&$query, &$conditions, $cgIndex){
  $nWhereGroup = count($conditions) + 1;

  foreach($conditions as $cIndex=>&$condition){
    if(isset($condition['field']) && strpos($condition['field'], 'field_categoria_cuscinetto.field_categoria_cuscinetto_target_id')){
      $currTid    = (int)array_values($condition['value'])[0];
      $childTids  = _productSearch_getCatCustinettiChilds($currTid);

      unset($query->where[$cgIndex]['conditions'][$cIndex]);
      if(count($childTids) > 0){
        $query->where[$nWhereGroup]['conditions'][] = [
          'field'     => 'node__field_categoria_cuscinetto.field_categoria_cuscinetto_target_id',
          'value'     => $childTids,
          'operator'  => 'IN',
        ];
        $query->where[$nWhereGroup]['type'] = 'OR';
      }
    }
  }
}

/**
 * Ritorna tutti i termini figli del termine attuale
 * Valido solo per il dizionario Categorie cuscinetti
 *
 * Aggiunge all'elenco anche se stesso
 */
function _productSearch_getCatCustinettiChilds($tid){
  $childsTids = [];
  $currTerm   = \Drupal::service('entity_type.manager')->getStorage("taxonomy_term")->load($tid);
  $currVoc    = $currTerm->getVocabularyId();

  if($currVoc === 'categorie_cuscinetti'){
    $childsTids[] = $tid;
    $childs       = \Drupal::entityManager()->getStorage('taxonomy_term')->loadTree('categorie_cuscinetti', $tid, NULL, FALSE);

    foreach($childs as $child){
      $childsTids[] = (int)$child->tid;
    }
  }

  return $childsTids;
}



// :node__field_categoria_cuscinetto_field_categoria_cuscinetto_target_id
// node__field_categoria_cuscinetto.field_categoria_cuscinetto_target_id = :node__field_categoria_cuscinetto_field_categoria_cuscinetto_target_id
