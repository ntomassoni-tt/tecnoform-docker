<?php

/**
 * Implements hook_theme().
 */
function weevo_utils_theme($existing, $type, $theme, $path) {
  $themePath = drupal_get_path('theme', 'tecnoform');
  $themes = [];

  // Plugin/Block/BannerFullsizeBlock
  $themes['banner_fullsize_block'] = [
    'variables' => [
      'bannerType'  => NULL,
      'pageTitle'   => NULL,
      'urls'        => NULL,
      'defBg'       => NULL,
    ]
  ];

  // Plugin/Block/GoogleMapsBlock
  $themes['google_maps_block'] = [
    'variables' => [
      'attributes'  => [],
    ],
  ];

  return $themes;
}
