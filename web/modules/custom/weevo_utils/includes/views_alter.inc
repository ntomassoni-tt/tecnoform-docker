<?php

use Drupal\Core\Form\FormStateInterface;
use Drupal\node\Entity\Node;
use Drupal\node\NodeInterface;

/**
 * hook_form_alter()
 *
 *
 * @param $form
 * @param FormStateInterface $form_state
 * @param $form_id
 */
function weevo_utils_form_alter(&$form, FormStateInterface $form_state, $form_id) {

  if($form_id === 'views_exposed_form') {
    $currView     = $form_state->getStorage('view');
    $viewId       = $currView['view']->id();
    $viewDisplay  = $currView['view']->getDisplay()->display['id'];


    // Vista Prodotti, display Ricerca prodotti
    /*
    if ($viewId === 'prodotti' && $viewDisplay === 'product_search') {
      $form['titolo']['#attributes']['placeholder'] = t('Product Code');
      $form['diametro_interno_min']['#attributes']['placeholder'] = t('Min');
      $form['diametro_interno_max']['#attributes']['placeholder'] = t('Max');
      $form['diametro_esterno_min']['#attributes']['placeholder'] = t('Min');
      $form['diametro_esterno_max']['#attributes']['placeholder'] = t('Max');
      $form['spessore_min']['#attributes']['placeholder']         = t('Min');
      $form['spessore_max']['#attributes']['placeholder']         = t('Max');

      //$form['categoria']['#options']['All']     = t('Category');
      //$form['subcategoria']['#options']['All']  = t('Subcategory');
      $form['gabbia']['#options']['All']        = t('Cage');
      $form['gioco']['#options']['All']         = t('Radial Internal Play');
      $form['schermatura']['#options']['All']   = t('Seal');
      $form['foro']['#options']['All']          = t('Bore');

      $form['actions']['submit']['#value']  = t('Search');
      $form['reset']['submit']['#value']    = t('Reset');
    }
     */
  }
}
