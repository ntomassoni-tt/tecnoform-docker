<?php

namespace Drupal\weevo_utils\Routing;

use Drupal\Core\Routing\RouteSubscriberBase;
use Symfony\Component\Routing\RouteCollection;
use Drupal\Core\Routing\RoutingEvents;

/**
 * Class WeevoRouteSubscriber
 *
 * Analizza la route della pagina attuale e reagisce a entity.taxonomy_term.canonical
 * Richiama il controller WeevoTaxTermViewPageController per procedere con l'eventuale visualizzazione dell'elenco prodotti
 */
class WeevoRouteSubscriber extends RouteSubscriberBase {

  /**
   *
   * {@inheritdoc}
   */
  protected function alterRoutes(RouteCollection $collection) {
    $route = $collection->get('entity.taxonomy_term.canonical');

    if($route){
      // $route->setDefault('_controller', '\Drupal\weevo_utils\Controller\WeevoTaxTermViewPageController::handle');
    }
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {
    $events = parent::getSubscribedEvents();

    //Eseguo dopo le Views
    $events[RoutingEvents::ALTER] = array('onAlterRoutes', -180);

    return $events;
  }

}
