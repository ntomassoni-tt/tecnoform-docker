<?php

/**
 * @file
 * Contains \Drupal\weevo_utils\Plugin\Block\GoogleMaps.
 */

namespace Drupal\weevo_utils\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Cache\Cache;

/**
 * Class GoogleMapsBlock
 *
 * @Block(
 *   id = "google_maps_block",
 *   admin_label = @Translation("Google Maps"),
 *   category = @Translation("Blocks"),
 * )
 */
class GoogleMapsBlock extends BlockBase {

  public function build() {
    return [
      '#theme'      => 'google_maps_block',
      '#attributes' => [],
      '#cache'      => ['contexts' => ['url.path']],
    ];
  }

}
