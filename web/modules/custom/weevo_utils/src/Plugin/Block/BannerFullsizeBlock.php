<?php

/**
 * @file
 * Contains \Drupal\weevo_utils\Plugin\Block\BannerFullsize.
 */

namespace Drupal\weevo_utils\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\node\Entity\Node;
use Drupal\image\Entity\ImageStyle;
use Drupal\Core\Cache\Cache;
use Drupal\Core\Entity\EntityInterface;
use Drupal\block\Entity\Block;
use Drupal\Core\Entity;
use Drupal\Core\Url;

/**
 * Definisce il blocco per la corretta visualizzazione del Banner
 * - Utilizza field diversi in base al tipo di contenuto
 * - Risale l'alberatura per ricavare un eventuale valore sul parent node
 * - Definisce una img di default
 *
 * @Block(
 *   id = "banner_fullsize_block",
 *   admin_label = @Translation("Banner Fullsize"),
 *   category = @Translation("Blocks"),
 * )
 */
class BannerFullsizeBlock extends BlockBase {

  private $themeImg;
  private $bannerType;
  private $pageTitle;
  private $urls;
  private $defBg;

  /**
   * {@inheritdoc}
   */
  public function build() {
    $currentPath  = \Drupal::service('path.current')->getPath();
    $drupalUrl    = Url::fromUserInput($currentPath);
    
    try {
      $routeName = $drupalUrl->getRouteName();
    }
    catch (\UnexpectedValueException $e) {
      $routeName    = 'entity.node.canonical';
    }

    $this->bannerType = NULL;
    $this->pageTitle  = NULL;
    $this->urls       = NULL;
    $this->themeImg   = '/'.\Drupal::theme()->getActiveTheme()->getPath().'/images/';
    $this->defBg      = $this->themeImg.'placeholder/banner-1920x360.jpg';

    switch ($routeName) {
      case 'entity.node.canonical':
        $node = \Drupal::routeMatch()->getParameter('node');
        if($node) { $this->buildForNode($node); }
        break;

      case 'entity.taxonomy_term.canonical':
        $term = \Drupal::routeMatch()->getParameter('taxonomy_term');
        if($term) { $this->buildForTerm($term); }
        break;

      default:
        $this->buildDefault();
        break;
    }

    return [
      '#theme'      => 'banner_fullsize_block',
      '#bannerType' => $this->bannerType,
      '#pageTitle'  => $this->pageTitle,
      '#urls'       => $this->urls,
      '#defBg'      => $this->defBg,
      '#cache'      => ['contexts' => ['url.path']]
    ];
  }

  /**
   * Default, per ora vuoto.
   * Non dovrebbe mai accadere
   */
  private function buildDefault() {}

  /**
   * Creo il banner per un nodo
   *
   * @param $node
   */
  private function buildForNode($node) {
    $this->bannerType = $node->getType();

    switch($this->bannerType){
      case 'page':
        $this->urls       = $this->buildForFieldBanner($node, true);
        $this->pageTitle  ='<div class="titleWrp">
                              <h1 class="page-title"><span class="field-wrapper">' . $node->getTitle() . '</span></h1>
                            </div>';
        break;

      case 'product':
        $catTid           = (!$node->field_categoria_cuscinetto->isEmpty()) ? $node->field_categoria_cuscinetto->getString() : 0;
        $termAncestor     = \Drupal::service('weevo_utils.term')->getFirstAncestorForTid($catTid);
        if($termAncestor) {
          $this->urls       = $this->buildForFieldBanner($termAncestor, false);
          $this->pageTitle  ='<div class="titleWrp">
                              <h1 class="page-title"><span class="field-wrapper">' . $node->getTitle() . '</span></h1>
                              <div class="contentTag"><span>'.$termAncestor->getName().'</span></div>
                            </div>';
        }
        break;
        
      case 'article':
        $catTid           = (!$node->field_blog_category->isEmpty()) ? $node->field_blog_category->getString() : 0;
        $termAncestor     = \Drupal::service('weevo_utils.term')->getFirstAncestorForTid($catTid);
        $date = $node->field_date->date;
        
        if($termAncestor) {
          $this->urls       = $this->buildForFieldBanner($node, true);
          $this->pageTitle  ='<div class="titleWrp">
                              <p class="articleDate">' . $date->format("d F Y") . '</p>
                              <h1 class="page-title"><span class="field-wrapper">' . $node->getTitle() . '</span></h1>
                              <div class="contentTag"><span>'.$termAncestor->getName().'</span></div>
                            </div>';
        }
        break;
    }
  }

  /**
   * Creo il banner per un termine di tassonomia
   * Per il vocabolario Categorie Cuscinetti vado a prendere il primo parent disponibile
   * Lo memorizzo (in modo fittizio) come node e genero le immagini con lo stesso script delle basic page
   * Questo perchè il termine ha sempre il banner in field_cover
   *
   * @param $term
   */
  private function buildForTerm($term) {
    $vid = $term->getVocabularyId();

    if($vid === 'categorie_cuscinetti'){
      $tid              = $term->id();
      $termAncestor     = \Drupal::service('weevo_utils.term')->getFirstAncestorForTid($tid);
      $this->bannerType = 'termCatCuscinetti';
      $this->urls       = $this->buildForFieldBanner($termAncestor, false);
      $this->defBg      = $this->themeImg.'placeholder/category-1920x360.jpg';
      $this->pageTitle  = '<div class="titleWrp">
                              <h1 class="page-title"><span class="field-wrapper">' . $term->getName() . '</span></h1>
                            </div>';
    } else { $this->buildDefault(); }
  }

  /**
   * Ricavo (se valorizzato) le immagini dal field "field_cover"
   *
   * @param $node
   * @param $checkParent
   * @return array|string
   */
  private function buildForFieldBanner($node, $checkParent){
    $urls   = '';
    $banner = ($node->hasField('field_cover') && !$node->get('field_cover')->isEmpty()) ? $node->get('field_cover') : null;
    $hideBanner = ($node->hasField('field_hide_banner') && !$node->get('field_hide_banner')->isEmpty()) ? $node->get('field_hide_banner')->value : null;

    if($hideBanner) {
      $urls = '';
    } elseif($banner !== null && !$banner->isEmpty()) {
      $imgUri = $banner->entity->getFileUri();
      $urls   = $this->imageCrop($imgUri);
      
      
    } elseif($checkParent) {
      /** @var \Drupal\Core\Menu\MenuLinkManagerInterface $menu_link_manager */
      $menuLinkManager = \Drupal::service('plugin.manager.menu.link');

      $urls = $this->buildForFieldBannerParent($menuLinkManager, $node->id());
    }

    return $urls;
  }

  /**
   * Analizza in modo ricorsivo l'alberatura dei parents nel menu per ricavare l'immagine
   *
   * @param \Drupal\Core\Menu\MenuLinkManagerInterface $menuLinkManager
   * @param $nid
   * @return array
   */
  private function buildForFieldBannerParent($menuLinkManager, $nid){
    $urls   = '';

    $links  = $menuLinkManager->loadLinksByRoute('entity.node.canonical', ['node' => $nid], 'main');
    $link   = reset($links);

    $hasParent = empty($link) ? false : true;
    if($hasParent){
      if($link->getParent() && $parent = $menuLinkManager->createInstance($link->getParent())){
        $route = $parent->getUrlObject()->getRouteParameters();

        if(isset($route['node']) && $parentNode = Node::load($route['node'])){
          if($parentNode && $parentNode->hasField('field_cover') && !$parentNode->get('field_cover')->isEmpty()){
            // Background image
            $banner = $parentNode->get('field_cover');
            $imgUri = $banner->entity->getFileUri();
            $urls   = $this->imageCrop($imgUri);
            return $urls;
          } else {
            $nodeId = $parentNode->id();
            $urls = $this->buildForFieldBannerParent($menuLinkManager, $nodeId);
          }
        }
      }
    }

    return $urls;
  }

  /**
   * Applico i crop adatti al field "Cover" (field_cover)
   *
   * @param $imgUri
   * @return array+
   */
  private function imageCrop($imgUri){
    return [
      'sm'  => ImageStyle::load('crop_1920x360')->buildUrl($imgUri),
      'md'  => ImageStyle::load('crop_1920x360')->buildUrl($imgUri),
      'lg'  => ImageStyle::load('crop_1920x360')->buildUrl($imgUri),
    ];
  }

}
