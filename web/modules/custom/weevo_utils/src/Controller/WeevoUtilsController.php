<?php

namespace Drupal\weevo_utils\Controller;

use Drupal\Core\Controller\ControllerBase;

class WeevoUtilsController extends ControllerBase {

  /**
   * Titolo per la pagina Prodotti
   * La pagina è aggiunta da weevo_utils.links.menu.yml
   *
   * @return mixed
   
  function productsTitle() {
    return $this->t('Products');
  }
   * 
   */

}
