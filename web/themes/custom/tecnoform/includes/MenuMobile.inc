<?php

use Drupal\Core\Link;
use Drupal\Core\Menu\MenuTreeParameters;
use Drupal\Core\Menu\InaccessibleMenuLink;

class MenuMobile{
  private $menuIds;

  /**
   * Costruttore della classe
   * Richiama ed elabora i menu
   * - main
   */
  public function __construct(){
    $this->menuIds  = array('main');
  }

  /**
   * Metodo di avvio per costruire il codice HTML del menu
   *
   * @return type
   */
  public function runMobileBuilder(){
    $menus    = array();
    foreach($this->menuIds as $menuId){ $menus[] = $this->loadMenuItems($menuId); }
    $fullMenu = $menus[0]; //array_merge($menus[0], $menus[1]);
    $codeMenu = $this->getCodeForMobileMenu($fullMenu);

    return $codeMenu;
  }

  /**
   * Carica l'intero menu ed applica la manipolazione dei link stessi
   * - Controlla i permessi di accesso al nodo
   * - Controlla i permessi di accesso alla voce di menu
   * - Ordina le voci in base al peso
   * Richiama inoltre la generazione dell'array associativo delle voci del menu stesse
   *
   * @param type $menuId
   * @return array
   */
  private function loadMenuItems($menuId){
    $menuArray  = array();

    $parameters = new MenuTreeParameters();
    $parameters->setMinDepth(0);
    $parameters->onlyEnabledLinks();

    $menuTreeService  = \Drupal::service('menu.link_tree');
    $treeLoaded       = $menuTreeService->load($menuId, $parameters);

    $manipulators = array(
      array('callable' => 'menu.default_tree_manipulators:checkNodeAccess'),
      array('callable' => 'menu.default_tree_manipulators:checkAccess'),
      array('callable' => 'menu.default_tree_manipulators:generateIndexAndSort'),
    );
    $tree = $menuTreeService->transform($treeLoaded, $manipulators);

    $this->generateSubMenuTree($menuId, $menuArray, $tree);

    return $menuArray;
  }

  /**
   * Genera in modo ricorsivo l'array associativo delle voci del menu
   *
   * @param type $output
   * @param type $input
   * @param string $parent
   */
  private function generateSubMenuTree($menuId, &$output, &$input, $parent = FALSE){
    if(is_array($input)){
      $input = array_values($input);
      
      $current_path = \Drupal::request()->getRequestUri();

      foreach($input as $key => $item){
        $active = false;
        
        if($item->link->isEnabled() && !$item->link instanceof InaccessibleMenuLink){
          $key        = 'submenu-'.$menuId."-".$key;
          $name       = $item->link->getTitle();
          $url        = $item->link->getUrlObject();
          $urlString  = $url->toString();
          $urlOptions = $url->getOption('attributes');
          $urlClass   = ($urlOptions && isset($urlOptions['class'])) ? (array)$urlOptions['class'] : [];
          
          if($item->inActiveTrail) {
            $urlClass[] = 'active-trail';
          }
          
          if ($urlString == $current_path) {
            // Add active link.
            $urlClass[] = 'Selected';
            $active = true;
          }

          $urlClsStr  = implode(" ", $urlClass);

          if(!$parent){
            $output[$key] = [
              'name'    => $name,
              'tid'     => $key,
              'urlStr'  => $urlString,
              'addCls'  => 'itm-menu-'.$menuId.' '.(string)$urlClsStr,
              'active'  => $active
            ];
          }else{
            $parent = 'submenu-' . $parent;
            $output['child'][$key] = [
              'name'    => $name,
              'tid'     => $key,
              'urlStr'  => $urlString,
              'addCls'  => 'itm-menu-'.$menuId.' '.$urlClsStr,
              'active'  => $active
            ];
          }

          if($item->hasChildren){
            if($item->depth == 1){ $this->generateSubMenuTree($menuId, $output[$key], $item->subtree, $key);
            }else{ $this->generateSubMenuTree($menuId, $output['child'][$key], $item->subtree, $key); }
          }
        }
      }
    }
  }

  /**
   * Avvia la generazione del codice HTML del menu mobile
   *
   * @param type $fullMenu
   * @return string
   */
  private function getCodeForMobileMenu($fullMenu){
    $code ='<nav id="mobile-menu">
              <ul>';

    foreach($fullMenu as $fIndex=>$fData){ $code.= $this->getCodeForMobileMenuRec($fData); }

    $code.='  </ul>
            </nav>';

    return $code;
  }

  /**
   * Genera in modo ricorsivo il codice HTML del link e degli eventuali child
   *
   * @param type $fData
   * @return string
   */
  private function getCodeForMobileMenuRec($fData){
    if(isset($fData['active']) && $fData['active']) {
      $code = '<li class="Selected">';
    } else {
      $code = '<li>';
    }

    if(array_key_exists('child', $fData)){
      $code.='<a href="'.$fData['urlStr'].'" title="'.$fData['name'].'" class="'.$fData['addCls'].'">'.$fData['name'].'</a>
              <ul>';

      foreach($fData['child'] as $ind=>$cData){
        $code.= $this->getCodeForMobileMenuRec($cData);
      }

      $code.='</ul>';
    }else{
      $code.= '<a href="'.$fData['urlStr'].'" title="'.$fData['name'].'" class="'.$fData['addCls'].'">'.$fData['name'].'</a>';
    }

    $code.= '</li>';

    return $code;
  }

}
