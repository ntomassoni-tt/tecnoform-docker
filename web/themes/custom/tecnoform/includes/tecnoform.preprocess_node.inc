<?php

use Drupal\taxonomy\Entity\Term;
use Drupal\file\Entity\File;
use Drupal\image\Entity\ImageStyle;
use Drupal\Core\Url;

/**
 * Implements template_preprocess_node().
 *
 * @param $variables
 */
function tecnoform_preprocess_node(&$variables){
  $node         = $variables['node'];
  $contentType  = $node->getType();
  $viewMode     = $variables['elements']['#view_mode'];
}
