<?php

use Drupal\Core\GeneratedLink;
use Drupal\Core\Url;

/**
 * Implements hook_theme_suggestions_html_alter().
 *
 * @param $suggestions
 * @param $variables
 */
function tecnoform_theme_suggestions_html_alter(&$suggestions, $variables) {
  $node = \Drupal::routeMatch()->getParameter('node');
  if (is_object($node)) { $suggestions[] = 'html__' . $node->getType(); }
}

/**
 * Implements hook_theme_suggestions_block_alter().
 *
 * http://kristiankaa.dk/article/drupal8-region-specific-menu-theme-hook-suggestion
 *
 * @param array $suggestions
 * @param array $variables
 */
function tecnoform_theme_suggestions_block_alter(array &$suggestions, array $variables){

  if(isset($variables['elements']['content']['#block_content'])) {
    $suggestions[] = 'block__' . $variables['elements']['content']['#block_content']->bundle();
  }

  if(isset($variables['elements']['content']['#type']) && $variables['elements']['content']['#type'] === 'webform'){
    $webform    = $variables['elements']['content']['#webform'];
    $webformId  = $webform->id();
    $suggestions[] = 'block__webform__' . $webformId;
  }
}

/**
 * Implements hook_theme_suggestions_input_alter().
 *
 * @param $suggestions
 * @param array $variables
 */
function tecnoform_theme_suggestions_input_alter(&$suggestions, array $variables) {
  $element = $variables['element'];

  if(isset($element['#attributes']['data-twig-suggestion'])){
    $suggestions[] = 'input__' . $element['#type'] . '__' . $element['#attributes']['data-twig-suggestion'];
  }
}

/**
 * Implements hook_theme_suggestions_taxonomy_term_alter().
 *
 * @param array $suggestions
 * @param array $variables
 */
function tecnoform_theme_suggestions_taxonomy_term_alter(array &$suggestions, array $variables) {
  /** @var \Drupal\taxonomy\TermInterface $term */
  $term = $variables['elements']['#taxonomy_term'];
  $sanitized_view_mode = strtr($variables['elements']['#view_mode'], '.', '_');

  // Add view mode theme suggestions.
  $suggestions[] = 'taxonomy_term__' . $sanitized_view_mode;
  $suggestions[] = 'taxonomy_term__' . $term->bundle() . '__' . $sanitized_view_mode;
  $suggestions[] = 'taxonomy_term__' . $term->id() . '__' . $sanitized_view_mode;
}

/**
 * Implements hook_theme_suggestions_views_view_alter().
 *
 * @param array $suggestions
 * @param array $variables
 */
function tecnoform_theme_suggestions_views_view_alter(array &$suggestions, array $variables) {
  $viewId = isset($variables['view']->element['#view_id']) ? (string)$variables['view']->element['#view_id'] : '';
  $viewDi = isset($variables['view']->element['#display_id']) ? (string)$variables['view']->element['#display_id'] : '';

  if($viewId !== ''){
    $suggestions[] = 'views_view__' . $viewId;
    if($viewDi !== ''){ $suggestions[] = 'views_view__' . $viewId . '__' . $viewDi; }
  }
}
