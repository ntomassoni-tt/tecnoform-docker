<?php

use Drupal\Component\Utility\Html;
use Drupal\Core\Url;
use Drupal\Core\Form\FormStateInterface;
use Drupal\user\Entity\User;
use Symfony\Component\HttpFoundation\RedirectResponse;

/**
 * Implements hook_form_alter().
 *
 * @param $form
 * @param $form_state
 * @param $form_id
 */
function tecnoform_form_alter(&$form, &$form_state, $form_id){
  if(!empty($form['actions']) && !empty($form['actions']['submit'])){
    unset($form['actions']['submit']['#attributes']['class']);
    $form['actions']['submit']['#attributes']['class'][] = 'button';
  }

  switch($form_id){
    case 'search_block_form':
      // Form di ricerca inserito nel footer
      $form['keys']['#attributes']['placeholder'] = t('Search');
      // $form['actions']['submit']['#value'] = Html::decodeEntities('&#xe843;');
      $form['actions']['submit']['#attributes']['class'][] = 'search-btn';
      break;

    case 'search_form':
      // Form di ricerca in pagina search

      unset($form['advanced']); // Rimuovo la ricerca avanzata
      unset($form['help_link']); // Rimuovo il link per help ricerca

      $form['tecnoform']['#prefix'] = '<div class="formMainWrapper">';
      $form['tecnoform']['#suffix'] = '</div>';

      $form['tecnoform']['keys']['#title'] = '';
      $form['tecnoform']['keys']['#attributes']['placeholder'] = t('Search');

      // $form['tecnoform']['submit']['#value'] = Html::decodeEntities('&#xe843;');
      $form['tecnoform']['submit']['#attributes']['class'][] = 'search-btn';
      break;

  }
}