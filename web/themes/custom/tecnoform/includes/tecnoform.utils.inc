<?php

use Drupal\Core\Link;
use Drupal\Core\Url;

/**
 * Basato su zurb_foundation_preprocess_page()
 *
 * Definisce le variabili per logo (header e footer) e numero verde
 *
 * @param type $variables
 */
function _tecnoform_processThemeLogos(&$variables){
  // Elementi comuni
  $siteName   = isset($variables['site_name']) ? $variables['site_name'] : \Drupal::config('system.site')->get('name');
  $siteTitle  = strip_tags($siteName);
  $fpUrl      = Url::fromRoute('<front>');
  $fpUrl->setOption('attributes', array('title' => $siteTitle, 'class' => array('logo')));

  // Logo
  $logo     = "/".\Drupal::theme()->getActiveTheme()->getPath()."/logo.svg";
  $logoImg  = array(
    '#theme'      => 'image',
    '#uri'        => $logo,
    '#attributes' => array(
      'alt'         => strip_tags($siteName),
      'title'       => strip_tags($siteName),
      'class'       => array('logo'),
    )
  );
  $variables['bp_logo'] = Link::fromTextAndUrl($logoImg, $fpUrl)->toString();

}
