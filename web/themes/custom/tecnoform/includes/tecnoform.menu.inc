<?php

/**
 * Implements hook_preprocess_menu().
 */
function tecnoform_preprocess_menu(&$vars, $hook) {
  foreach ($vars['items'] as $item) {
    /** @var \Drupal\Core\Menu\MenuLinkDefault $menu_link */
    $menu_link = $item['original_link'];
    $options = $menu_link->getOptions();

    // Apply container attributes on <li> element.
    if ($options && isset($options['container_attributes'])) {
      foreach ($options['container_attributes'] as $attribute => $value) {
        $item['attributes']->setAttribute($attribute, $value);
      }
    }
  }
}

/**
 * Set active and active-trail class for sub-menus recursively.
 */
function tecnoform_menu_process_submenu(&$submenu, $current_path) {
  foreach($submenu as &$item){
    if($item['in_active_trail']){
      if($item['url']->toString() == $current_path){
        $item['is_active'] = TRUE;
      }elseif(count($item['below'])){
        tecnoform_menu_process_submenu($item['below'], $current_path);
      }
    }
  }
}

/**
 * Implements theme_preprocess_menu_local_task().
 */
function tecnoform_preprocess_menu_local_task(&$variables) {
  $output = '';

  if (!empty($variables['primary'])) {
    $variables['primary']['#prefix'] = '<h2 class="visually-hidden">' . t('Primary tabs') . '</h2>';
    $variables['primary']['#prefix'] .= '<div class="menu button-group">';
    $variables['primary']['#suffix'] = '</div>';
    $output .= drupal_render($variables['primary']);
  }

  if (!empty($variables['secondary'])) {
    $variables['secondary']['#prefix'] = '<h2 class="visually-hidden">' . t('Secondary tabs') . '</h2>';
    $variables['secondary']['#prefix'] .= '<div class="menu button-group">';
    $variables['secondary']['#suffix'] = '</div>';
    $output .= drupal_render($variables['secondary']);
  }

  return $output;
}

/**
 * Implements hook_theme_suggestions_HOOK_alter().
 *
 * Per suggestion sui menu
 * http://kristiankaa.dk/article/drupal8-region-specific-menu-theme-hook-suggestion
 */
function tecnoform_theme_suggestions_menu_alter(array &$suggestions, array $variables) {
  // Remove the block and replace dashes with underscores in the block ID to use for the hook name.
  if(isset($variables['attributes']['block'])){
    $hook = str_replace(array('block-', '-'), array('', '_'), $variables['attributes']['block']);
    $suggestions[] = $variables['theme_hook_original'] . '__' . $hook;
  }
}
