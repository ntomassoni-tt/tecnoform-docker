<?php

use Drupal\Core\GeneratedLink;
use Drupal\Core\Url;

/**
 * Implements template_preprocess_html().
 *
 * Adds additional classes
 *
 * @param $variables
 */
function tecnoform_preprocess_html(&$variables){
  //Add body class for user id
  $myUserId = \Drupal::currentUser()->id();
  $variables['attributes']['class'][] = 'user-'.$myUserId;

  if(($node = \Drupal::routeMatch()->getParameter('node')) && $node instanceof \Drupal\node\NodeInterface){
    $variables['attributes']['class'][] = 'node-'.$node->id();
  }
}

/**
 * Implements template_preprocess_page().
 *
 * Manage various additional datas
 *
 * @param $variables
 */
function tecnoform_preprocess_page(&$variables) {
  // Theme path nei settings di pagina
  $variables['#attached']['drupalSettings']['path']['themeUrl'] = \Drupal::theme()->getActiveTheme()->getPath();

  // Ricava il codice per il menu mobile (stampato in templates/partial/asides.html.twig)
  $mmenuBuilder             = new MenuMobile();
  $variables["mmenu_code"]  = $mmenuBuilder->runMobileBuilder();

  //Gestione del logo SVG stampato in header e degli elementi in footer
  _tecnoform_processThemeLogos($variables);

  //Preprocess del nodo per gestire variabili di comodo
  $node = \Drupal::routeMatch()->getParameter('node');

  if (is_object($node)) {
    switch($node->getType()){}
  }

  // Remove classes to highlighted region.
  if (!empty($variables['page']['highlighted'])) {
    unset($variables['page']['highlighted']['#attributes']['class']);
    $variables['page']['highlighted']['#attributes']['class'][] = 'region-highlighted';
  }

  // Convenience variables
  if(isset($variables['page']['sidebar_first'])){ $left = $variables['page']['sidebar_first']; }

  // Dynamic sidebars
  if(!empty($left)){
    $variables['main_grid']           = 'medium-9 medium-push-3';
    $variables['sidebar_first_grid']  = 'medium-3 medium-pull-9';
  }else{
    $variables['main_grid']           = 'large-12';
    $variables['sidebar_first_grid']  = '';
  }

}

/**
 * Implements hook_preprocess_block().
 *
 * @param $variables
 */
function tecnoform_preprocess_block(&$variables){
  //$variables['content']['#attributes']['block'] = $variables['attributes']['id'];
}

/**
 * Implements hook_preprocess_links__language_block().
 *
 * - rimuove le lingue che non hanno traduzione
 *
 * @param $variables
 */
function tecnoform_preprocess_links__language_block(&$variables) {

  $node = \Drupal::routeMatch()->getParameter('node');
  $checkTrans = ($node) ? true : false;

  foreach(array_keys($variables["links"]) as $lang) {
    if($checkTrans && is_object($node) && !$node->hasTranslation($lang) && $lang !== 'activeLink') {
      unset($variables['links'][$lang]);
    }
  }
}

/**
 * Implements template_preprocess_field().
 *
 * @param $variables
 * @param $hook
 */
function tecnoform_preprocess_field(&$variables, $hook){}

/**
 * Implements hook_preprocess_breadcrumb().
 * Adds the "title" variable so that the current page can be added as a breadcrumb.
 *
 * @param $variables
 */
function tecnoform_preprocess_breadcrumb(&$variables){
  $request = \Drupal::request();
  $route_match = \Drupal::routeMatch();
  $title = \Drupal::service('title_resolver')->getTitle($request, $route_match->getRouteObject());
  //unset($variables['title']);
}
