<?php

/**
 * Implements template_preprocess_taxonomy_term().
 *
 * @param $variables
 */
function tecnoform_preprocess_taxonomy_term(&$variables){
  $term         = $variables['elements']['#taxonomy_term'];
  $vocabulary   = $term->getVocabularyId();
  $viewMode     = $variables['elements']['#view_mode'];
}
