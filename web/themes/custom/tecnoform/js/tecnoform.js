(function ($, Drupal) {

  /**
   * Funzione d'appoggio
   * Richiamata con il parametro da cercare
   */
  function getQueryString(key){
    key = key.replace(/[*+?^$.\[\]{}()|\\\/]/g, "\\$&"); // escape RegEx meta chars
    var match = location.search.match(new RegExp("[?&]"+key+"=([^&]+)(&|$)"));
    return match && decodeURIComponent(match[1].replace(/\+/g, " "));
  }

  /**
   * Funzione d'appoggio
   * Determina se IE11
   * https://stackoverflow.com/a/27069089
   *
   * @returns {number}
   */
  function ieVersion(){
    var ua = window.navigator.userAgent;
    if(ua.indexOf("Trident/7.0") > -1){ return 11;
    }else if(ua.indexOf("Trident/6.0") > -1){ return 10;
    }else if (ua.indexOf("Trident/5.0") > -1){ return 9; }

    return 0;  // not IE9, 10 or 11
  }

  /**
   * Funzione d'appoggio
   * Clona il cambio lingua da mettere nel mobile menu
   *
   * @returns {*}
   */
  function cloneLangSwitcher() {
    if($("#block-selettorelingua ul.dropdown-menu").length){ return $("#block-selettorelingua ul.dropdown-menu").html();
    }else{ return ""; }
  }


  Drupal.behaviors.globalPageBehavior = {
    attach: function (context, settings) {
      $('body', context).once('globalPageBehavior').each(function(){});
    }
  };


  /**
   * Behavior per mmenu
   */
  Drupal.behaviors.mobileMenuBehavior = {
    attach: function (context, settings){
      $('main', context).once('mobileMenuBehavior').each(function(){
        if($("#mobile-menu").length === 1){
          $("#mobile-menu").mmenu({
            extensions: ["multiline"],
            counters: false,
            navbar: { "add" : true, "title": null},
            "navbars": [
              {
                "position": "bottom",
                "content": ["<ul id='mobilelang'>"+cloneLangSwitcher()+"</div>"],
                "height":	2
              }
            ]
          }, { selectedClass: "Selected", });

          var API = $("#mobile-menu").data("mmenu");

          API.bind('close:finish', function(){ $(".hamburger").removeClass('is-active'); });
          API.bind('open:finish', function(){ $(".hamburger").addClass('is-active'); });

          $(".hamburger").click(function(){
            if($(this).hasClass('is-active')){ API.close();
            }else{ API.open(); }
          });
        }
      });
    }
  };

  /**
   * Behavior per outdatedBrowser
   * TODO - configurare eventuali lingue del sito
   */
  Drupal.behaviors.outdatedBrowserBehaviors = {
    attach: function (context, settings) {
      $('body', context).once('outdatedBrowserBehaviors').each(function(){

        var myLang    = ($('body.page-lang-en').length > 0) ? 'en' : 'it',
            themePath = drupalSettings.path.baseUrl + drupalSettings.path.themeUrl,
            outdPath  = themePath + "/vendors/outdatedbrowser/lang/",
            langPath  = outdPath + myLang + ".html";

        outdatedBrowser({
          bgColor:      '#2E2E2E',
          color:        '#ffffff',
          lowerThan:    'object-fit',
          languagePath: langPath
        });
      });
    }
  };


})(jQuery, Drupal);



